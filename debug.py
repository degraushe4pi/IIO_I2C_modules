DEBUG = False

def set_debug_on():
    global DEBUG
    DEBUG = True
    return DEBUG

def set_debug_off():
    global DEBUG
    DEBUG = False
    return DEBUG

def get_debug():
    global DEBUG
    return DEBUG

