from pathlib import Path
# list iio attached devices
p = Path('/sys/bus/iio/devices')
for device in list(p.glob('iio:device?')):
    # sensor name 
    with open( str(device) + '/name', 'r') as name:
        sensor_name = name.read().strip()
        print( 'Sensor found: ', sensor_name )
exit()
