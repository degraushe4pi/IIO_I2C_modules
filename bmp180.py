from . import get_debug

def read_bmp180(NN=None):
    """Reading values from BMP180 sensor.

    required: /boot/config.txt - dtoverlay=i2c-sensor,bmp180
    parameter: local altitude - allowed numeric input values are -500..9000
    returns an array with temperature and pressure 
    """         
    DEBUG = get_debug()
    # check parameters
    if NN is not None:
        if type(NN) is not int or NN < -500 or NN > 9000:
            raise TypeError("NN value must be -500..9000")
    #
    from pathlib import Path
    # check iio attached devices
    p = Path('/sys/bus/iio/devices')
    for device in list(p.glob('iio:device?')):
        # sensor name = bmp180 ?
        with open( str(device) + '/name', 'r') as name:
            sensor_name = name.read().strip()
            if sensor_name != 'bmp180':
                if DEBUG: print( 'Sensor skipped   : ', sensor_name )
                continue
            if DEBUG: print( 'Sensor found     : ', sensor_name )
        # read temperature
        with open( str(device) + '/in_temp_input', 'r') as value:
            temperature = float(value.read()) / 1000
            if DEBUG: print( 'Temperature in °C: ', temperature )
        # read pressure
        with open( str(device) + '/in_pressure_input', 'r') as value:
            pressure = float(value.read()) * 10
            if DEBUG: print( 'Pressure in hPa  : ', pressure )
        # pressure at sea level
        if NN is not None:
            pressure = pressure / pow( ( 273.15 + temperature ) /
                       ( 273.15 + temperature + 0.0065 * NN ), 5.255 )
            if DEBUG: print( 'Pressure NN hPa  : ', pressure )
        # bmp180 done
        return temperature, pressure
    # not found -> return None
    if DEBUG: print ('sensor BMP180 not found !')
    return
