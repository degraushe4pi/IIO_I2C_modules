def read_ads1x15():
    """
    Reading values from ADS1015 or ADS1115 - GND<->A0,A1,A2,A3
    ADS1015 gain: 0=3x,1=2x,2=1x,3=0.5x,4=0.25x,5=0.125x
    ADS1115 gain: 0=0.1875x,1=0.125x,2=0.00625x,3=0.03125x,4=0.015625x,5=0.007813x
    required: /boot/config.txt entry - 
    dtoverlay=ads1015 or ads1115
    dtparam=cha_enable,cha_cfg=4,cha_gain=0
    dtparam=chb_enable,chb_cfg=5,chb_gain=1
    dtparam=chc_enable,chc_cfg=6,chc_gain=2
    dtparam=chd_enable,chd_cfg=7,chd_gain=3

    """         
    DEBUG = True
    #
    from pathlib import Path
    # check iio attached devices
    p = Path('/sys/bus/iio/devices')
    for device in list(p.glob('iio:device?')):
        # sensor name = ads1015 reported for both types
        with open( str(device) + '/name', 'r') as name:
            sensor_name = name.read().strip()
            if sensor_name != 'ads1015':
                if DEBUG: print( 'Sensor skipped   : ', sensor_name )
                continue
            if DEBUG: print( 'Sensor found     : ', sensor_name )
        #
        A=[]
        for i in range(4):
            if DEBUG: print( 'ADS input A' + str(i) )
            # read voltage_raw mV
            with open( str(device) + '/in_voltage' + str(i) + '_raw', 'r') as value:
                voltage_raw = int(value.read())
                if DEBUG: print( 'voltage raw in mV: ', voltage_raw )
            # read scale
            with open( str(device) + '/in_voltage' + str(i) + '_scale', 'r') as value:
                voltage_scale = float(value.read()) 
                if DEBUG: print( 'voltage scale: ', voltage_scale )
            # value in V
            voltage = ( voltage_raw * voltage_scale ) / 1000 
            if DEBUG: print( 'voltage in V: ', voltage )
            A.append(voltage)
        # ads1015 done
        return A
    # not found -> return None
    if DEBUG: print ('sensor ADS1x15 not found !')
    return
